package br.com.pedrociarlini.enotas.client;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.ws.rs.InternalServerErrorException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.pedrociarlini.enotas.client.config.CustomResteasyJackson2Provider;
import br.com.pedrociarlini.enotas.client.exception.EnotasHandledException;
import br.com.pedrociarlini.enotas.client.vo.ENotasClienteVO;
import br.com.pedrociarlini.enotas.client.vo.ENotasMunicipioVO;
import br.com.pedrociarlini.enotas.client.vo.ENotasProdutoVO;
import br.com.pedrociarlini.enotas.client.vo.ENotasVendaVO;
import br.com.pedrociarlini.enotas.client.vo.ENotasVendaVO.VendasClienteId;
import br.com.pedrociarlini.enotas.client.vo.EnotasClienteId;
import br.com.pedrociarlini.enotas.client.vo.EnotasVendaId;

class ENotasFacadeTest {

	private static ENotasFacade facade;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		facade = ENotasRestClientFactory.createEnotasFacade("NTk4ZjhiYTMtMWMzOC00NWJhLThlZmEtNDQxZjIxNzVkMzI3");

		// FIXME Usar uma URL de serviço MOCK
		// facade = ENotasRestClientFactory.createEnotasFacade("", "http://localhost:3333/bla");
	}

	@Test
	void testObterCliente() {
		EnotasClienteId result = facade.insereOuAtualizaCliente(new ENotasClienteVO("pedrociarlini@gmail.com"));
		assertNotNull(result);
		assertEquals("ee16394a-b44f-46cf-931f-4736de0b0700", result.getClienteId());
	}

	/**
	 * Retorna
	 * 
	 * <pre>
	 * {
	"vendaId" : "b6c40d73-d657-4d08-ab24-84746f3a0700"
	}
	 * </pre>
	 * 
	 * onde b6c40d73-d657-4d08-ab24-84746f3a0700 = id da venda, e varia.
	 * 
	 * @throws JsonProcessingException
	 */
	@Test
	void testEmitirNota_happyDay() throws JsonProcessingException {
		ENotasVendaVO venda = createNota();
		try {
			EnotasVendaId result = facade.emitirNota(venda);
			assertNotNull(result);
			System.out.println(CustomResteasyJackson2Provider.mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result));

		} catch (InternalServerErrorException e) {
			EnotasHandledException ex = e.getResponse().readEntity(EnotasHandledException.class);
			System.err.println(ex.getMessage());
			Assertions.fail(e);
		}
	}

	private ENotasVendaVO createNota() {
		String nomeProduto = "Produto de Teste";
		String tags = "cursos teste";
		BigDecimal valorTotal = BigDecimal.valueOf(1.00);
		LocalDate hoje = LocalDate.now();

		ENotasProdutoVO produto = new ENotasProdutoVO(nomeProduto);
		// EnotasClienteId clienteId = facade.obterCliente(new ENotasClienteVO("pedrociarlini@gmail.com"));

		EnotasClienteId clienteId = new EnotasClienteId("ee16394a-b44f-46cf-931f-4736de0b0700"); // ID do Pedro ciarlini

		ENotasVendaVO nota = new ENotasVendaVO();
		nota.setCliente(new VendasClienteId(clienteId.getClienteId()));

		nota.setProduto(produto);
		produto.setIdExterno("" + 32716); // Curso do Pedro Ciarlini
		produto.setValorTotal(valorTotal);
		produto.setDiasGarantia(1);
		produto.setTags(tags);

		nota.setData(hoje);
		nota.setVencimento(hoje);
		nota.setValorTotal(BigDecimal.valueOf(1.00));
		nota.setQuandoEmitirNFe(0);
		nota.setEnviarNFeCliente(true);
		nota.setMeioPagamento(0);
		nota.setTags(tags);
		nota.setMunicipioPrestacao(ENotasMunicipioVO.MUNICIPIO_FORTALEZA);
		nota.setDataCompetencia(hoje);
		nota.setDiscriminacao(nomeProduto);
		nota.setValorTotalNFe(valorTotal);
		nota.setIssRetidoFonte(false);
		return nota;
	}
}
