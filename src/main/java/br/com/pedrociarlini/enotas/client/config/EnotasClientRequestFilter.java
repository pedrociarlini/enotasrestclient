package br.com.pedrociarlini.enotas.client.config;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.ext.Provider;

import com.google.common.net.HttpHeaders;

@Provider
public class EnotasClientRequestFilter implements ClientRequestFilter {

	private String apiKey;

	public EnotasClientRequestFilter() {
		throw new IllegalArgumentException("Use o construtor EnotasClientRequestFilter(String apiKey).");
	}

	public EnotasClientRequestFilter(String apiKey) {
		this.apiKey = apiKey;
	}

	@Override
	public void filter(ClientRequestContext reqContext) throws IOException {
		reqContext.getHeaders().add(HttpHeaders.AUTHORIZATION, "Basic " + apiKey);
		System.out.println(CustomResteasyJackson2Provider.mapper.writerWithDefaultPrettyPrinter().writeValueAsString(reqContext.getEntity()));

	}
}
