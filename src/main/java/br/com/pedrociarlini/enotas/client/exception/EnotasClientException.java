package br.com.pedrociarlini.enotas.client.exception;

public class EnotasClientException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EnotasClientException() {
		super();
	}

	public EnotasClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public EnotasClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public EnotasClientException(String message) {
		super(message);
	}

	public EnotasClientException(Throwable cause) {
		super(cause);

	}

}
