package br.com.pedrociarlini.enotas.client.vo;

import java.io.Serializable;

public class ENotasMunicipioVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nome;
	private Integer codigoIbge;

	public static final ENotasMunicipioVO MUNICIPIO_FORTALEZA = new ENotasMunicipioVO(2304400, "Fortaleza");

	public ENotasMunicipioVO(int codigo, String nome) {
		this();
		this.nome = nome;
		this.codigoIbge = codigo;
	}

	public ENotasMunicipioVO() {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCodigoIbge() {
		return codigoIbge;
	}

	public void setCodigoIbge(Integer codigoIbge) {
		this.codigoIbge = codigoIbge;
	}

	@Override
	public String toString() {
		return "ENotasMunicipioVO [nome=" + nome + ", codigoIbge=" + codigoIbge + "]";
	}
}
