package br.com.pedrociarlini.enotas.client.config;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class EnotasClientResponseFilter implements ClientResponseFilter {

	public EnotasClientResponseFilter() {
	}

	@Override
	public void filter(ClientRequestContext requestContext, ClientResponseContext respContext) throws IOException {
		// if (respContext.getStatus() == HttpStatus.SC_INTERNAL_SERVER_ERROR && respContext.hasEntity()) {
		// EnotasClientException ex = CustomResteasyJackson2Provider.mapper.readValue(respContext.getEntityStream(), EnotasHandledException.class);
		//
		// throw ex;
		//
		// }
	}
}
