package br.com.pedrociarlini.enotas.client;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.pedrociarlini.enotas.client.vo.ENotasClienteVO;
import br.com.pedrociarlini.enotas.client.vo.ENotasVendaVO;
import br.com.pedrociarlini.enotas.client.vo.EnotasClienteId;
import br.com.pedrociarlini.enotas.client.vo.EnotasVendaId;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface ENotasFacade {

	/**
	 * Insere ou atualiza um cliente.
	 * 
	 * @param nota
	 * @return identificador único do cliente
	 */
	@POST
	@Path("clientes")
	public EnotasClienteId insereOuAtualizaCliente(ENotasClienteVO nota);

	/**
	 * Retorna
	 * 
	 * <pre>
	 * {
	"vendaId" : "b6c40d73-d657-4d08-ab24-84746f3a0700"
	}
	 * </pre>
	 * 
	 * onde b6c40d73-d657-4d08-ab24-84746f3a0700 = id da venda, e varia.
	 * 
	 * @param nota
	 * @return
	 */
	@POST
	@Path("vendas")
	public EnotasVendaId emitirNota(ENotasVendaVO nota);
}