package br.com.pedrociarlini.enotas.client.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

public class ENotasVendaVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private VendasClienteId cliente;

	/**
	 * 'd/M/y'
	 */
	private LocalDate data;
	private LocalDate vencimento;

	private ENotasProdutoVO produto;

	private BigDecimal valorTotal;

	private Integer quandoEmitirNFe;

	private Boolean enviarNFeCliente;
	private Integer meioPagamento;

	private String tags;
	private ENotasMunicipioVO municipioPrestacao;

	private LocalDate dataCompetencia;
	private String discriminacao;
	private BigDecimal valorTotalNFe;

	private BigDecimal aliquotaIss;

	private BigDecimal valorIss;
	private Boolean issRetidoFonte;
	private BigDecimal deducoes;
	private String observacoes;

	public ENotasVendaVO() {
	}

	public VendasClienteId getCliente() {
		return cliente;
	}

	public void setCliente(VendasClienteId cliente) {
		this.cliente = cliente;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public LocalDate getVencimento() {
		return vencimento;
	}

	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}

	public ENotasProdutoVO getProduto() {
		return produto;
	}

	public void setProduto(ENotasProdutoVO produto) {
		this.produto = produto;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Integer getQuandoEmitirNFe() {
		return quandoEmitirNFe;
	}

	public void setQuandoEmitirNFe(Integer quandoEmitirNFe) {
		this.quandoEmitirNFe = quandoEmitirNFe;
	}

	public Boolean getEnviarNFeCliente() {
		return enviarNFeCliente;
	}

	public void setEnviarNFeCliente(Boolean enviarNFeCliente) {
		this.enviarNFeCliente = enviarNFeCliente;
	}

	public Integer getMeioPagamento() {
		return meioPagamento;
	}

	public void setMeioPagamento(Integer meioPagamento) {
		this.meioPagamento = meioPagamento;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tag) {
		this.tags = tag;
	}

	public ENotasMunicipioVO getMunicipioPrestacao() {
		return municipioPrestacao;
	}

	public void setMunicipioPrestacao(ENotasMunicipioVO municipioPrestacao) {
		this.municipioPrestacao = municipioPrestacao;
	}

	public LocalDate getDataCompetencia() {
		return dataCompetencia;
	}

	public void setDataCompetencia(LocalDate dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	public String getDiscriminacao() {
		return discriminacao;
	}

	public void setDiscriminacao(String discriminacao) {
		this.discriminacao = discriminacao;
	}

	public BigDecimal getValorTotalNFe() {
		return valorTotalNFe;
	}

	public void setValorTotalNFe(BigDecimal valorTotalNFe) {
		this.valorTotalNFe = valorTotalNFe;
	}

	public BigDecimal getAliquotaIss() {
		return aliquotaIss;
	}

	public void setAliquotaIss(BigDecimal aliquotaIss) {
		this.aliquotaIss = aliquotaIss;
	}

	public BigDecimal getValorIss() {
		return valorIss;
	}

	public void setValorIss(BigDecimal valorIss) {
		this.valorIss = valorIss;
	}

	public BigDecimal getDeducoes() {
		return deducoes;
	}

	public void setDeducoes(BigDecimal deducoes) {
		this.deducoes = deducoes;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	// Nested classes

	public Boolean getIssRetidoFonte() {
		return issRetidoFonte;
	}

	public void setIssRetidoFonte(Boolean issRetidoFonte) {
		this.issRetidoFonte = issRetidoFonte;
	}

	@Override
	public String toString() {
		return "ENotasVendaVO [cliente=" + cliente + ", data=" + data + ", vencimento=" + vencimento + ", produto=" + produto + ", valorTotal="
				+ valorTotal + ", quandoEmitirNFe=" + quandoEmitirNFe + ", enviarNFeCliente=" + enviarNFeCliente + ", meioPagamento=" + meioPagamento
				+ ", tag=" + tags + ", municipioPrestacao=" + municipioPrestacao + ", dataCompetencia=" + dataCompetencia + ", discriminacao="
				+ discriminacao + ", valorTotalNFe=" + valorTotalNFe + ", aliquotaIss=" + aliquotaIss + ", valorIss=" + valorIss + ", issRetidoFonte="
				+ issRetidoFonte + ", deducoes=" + deducoes + ", observacoes=" + observacoes + "]";
	}

	public static class VendasClienteId {

		private String id;

		public VendasClienteId(String clienteId) {
			this.setId(clienteId);
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		@Override
		public String toString() {
			return "VendasClienteId [id=" + id + "]";
		}

	}

}
