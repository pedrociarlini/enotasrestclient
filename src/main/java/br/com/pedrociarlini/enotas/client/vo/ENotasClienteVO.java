package br.com.pedrociarlini.enotas.client.vo;

/**
 * Para ser usada na consulta ao WebService do Enotas.
 * 
 * @author pedrociarlini
 *
 */
public class ENotasClienteVO {
	private EnotasClienteId clienteId;

	private String nomeFantasia;
	private String inscricaoMunicipal;
	private String inscricaoEstadual;
	private String email;
	private String telefone;
	private String nome;
	private String cpfCnpj;
	private ENotasEnderecoVO endereco;

	public ENotasClienteVO(String email) {
		this();
		this.email = email;
	}

	public ENotasClienteVO() {
	}

	public EnotasClienteId getClienteId() {
		return clienteId;
	}

	public void setClienteId(EnotasClienteId clienteId) {
		this.clienteId = clienteId;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public ENotasEnderecoVO getEndereco() {
		return endereco;
	}

	public void setEndereco(ENotasEnderecoVO endereco) {
		this.endereco = endereco;
	}

	@Override
	public String toString() {
		return "ENotasClienteVO [clienteId=" + getClienteId() + ", email=" + email + ", cpfCnpj=" + cpfCnpj + "]";
	}
}
