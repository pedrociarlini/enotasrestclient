package br.com.pedrociarlini.enotas.client;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import br.com.pedrociarlini.enotas.client.config.CustomResteasyJackson2Provider;
import br.com.pedrociarlini.enotas.client.config.EnotasClientRequestFilter;

public class ENotasRestClientFactory {
	public static final String DEFAULT_URL = "https://app.enotas.com.br/api";

	/**
	 * Usa a URL de produção do Enotas: {@link #DEFAULT_URL}
	 * 
	 * @param apiKey
	 * @return
	 */
	public static ENotasFacade createEnotasFacade(String apiKey) {
		return createEnotasFacade(apiKey, DEFAULT_URL);
	}

	/**
	 * 
	 * @param apiKey
	 * @param url    não pode ser nulo.
	 * @return
	 */
	public static ENotasFacade createEnotasFacade(String apiKey, String url) {
		if (url == null)
			throw new IllegalArgumentException("A [url] do webservice do Enotas não pode ser nulla.");

		ResteasyClientBuilder clientBuilder = new ResteasyClientBuilder();
		clientBuilder.register(CustomResteasyJackson2Provider.class);
		clientBuilder.register(new EnotasClientRequestFilter(apiKey));
		// clientBuilder.register(EnotasClientResponseFilter.class);

		ResteasyClient client = (ResteasyClient) clientBuilder.build();
		ResteasyWebTarget rtarget = (ResteasyWebTarget) client.target(url);

		return rtarget.proxy(ENotasFacade.class);
	}
}
