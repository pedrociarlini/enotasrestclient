package br.com.pedrociarlini.enotas.client.exception;

import com.fasterxml.jackson.annotation.JsonAlias;

/**
 * Denota exceções que puderam ser tratadas.
 * 
 * @author pedrociarlini
 *
 */
public class EnotasHandledException extends EnotasClientException {

	private static final long serialVersionUID = 1L;
	
	@JsonAlias("Message")
	private String errorMessage;

	public EnotasHandledException() {
	}

	public EnotasHandledException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public EnotasHandledException(String message, Throwable cause) {
		super(message, cause);
	}

	public EnotasHandledException(String message) {
		super(message);
	}

	public EnotasHandledException(Throwable cause) {
		super(cause);
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
