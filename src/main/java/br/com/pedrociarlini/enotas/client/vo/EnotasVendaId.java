package br.com.pedrociarlini.enotas.client.vo;

import java.io.Serializable;

public class EnotasVendaId implements Serializable {

	private static final long serialVersionUID = 1L;

	private String vendaId;

	public String getVendaId() {
		return vendaId;
	}

	public void setVendaId(String vendaId) {
		this.vendaId = vendaId;
	}

	@Override
	public String toString() {
		return "EnotasVendaId [vendaId=" + vendaId + "]";
	}
}
