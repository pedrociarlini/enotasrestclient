package br.com.pedrociarlini.enotas.client.config;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatterBuilder;

import org.jboss.resteasy.plugins.providers.jackson.ResteasyJackson2Provider;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

public class CustomResteasyJackson2Provider extends ResteasyJackson2Provider {
	public static final ObjectMapper mapper = new ObjectMapper();

	static {
		//		mapper.registerModule(new JavaTimeModule());
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
		mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		mapper.registerModule(new JavaTimeModule().addSerializer(LocalDate.class,
				new LocalDateSerializer(new DateTimeFormatterBuilder().appendPattern("dd/MM/yyyy").toFormatter())));
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		mapper.setDateFormat(sdf);

	}

	public CustomResteasyJackson2Provider() {

		setMapper(mapper);
	}
}
