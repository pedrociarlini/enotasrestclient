package br.com.pedrociarlini.enotas.client.vo;

public class EnotasClienteId {

	private String clienteId;

	public EnotasClienteId(String clienteId) {
		this();
		this.clienteId = clienteId;
	}

	public EnotasClienteId() {
	}

	/**
	 * É para ser o CPF do cliente
	 * 
	 * @return
	 */
	public String getClienteId() {
		return clienteId;
	}

	public void setClienteId(String clienteId) {
		this.clienteId = clienteId;
	}

	@Override
	public String toString() {
		return "EnotasClienteId [clienteId=" + clienteId + "]";
	}
}
