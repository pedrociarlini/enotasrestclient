package br.com.pedrociarlini.enotas.client.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ENotasProdutoVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nome;
	private String idExterno;
	private BigDecimal valorTotal;
	private Integer diasGarantia;
	private String tags;

	public ENotasProdutoVO(String nomeProduto) {
		this();
		this.nome = nomeProduto;
	}

	public ENotasProdutoVO() {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIdExterno() {
		return idExterno;
	}

	public void setIdExterno(String idExterno) {
		this.idExterno = idExterno;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Integer getDiasGarantia() {
		return diasGarantia;
	}

	public void setDiasGarantia(Integer diasGarantia) {
		this.diasGarantia = diasGarantia;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}
}
